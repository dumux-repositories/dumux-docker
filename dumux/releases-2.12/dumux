#!/usr/bin/env bash

# the image name
PUB_IMAGE_NAME=dumux/release-2.12
# the host directory that is mounted into...
SHARED_DIR_HOST="$(pwd)"
# ...this container directory.
SHARED_DIR_CONTAINER="/dumux/shared"

executeandlog ()
{
    echo "[$@]"
    eval $@
}

help ()
{
    echo ""
    echo "Usage: dumux <command> [options]"
    echo ""
    echo "  dumux open [image]      - open a DuMuX Docker container."
    echo "  dumux help              - display this message."
    echo ""
    echo "Optionally supply an Docker image name to the open command."
    echo ""
}

# open the dumux container. Only argument is the Docker image.
open()
{
    IMAGE="$1"
    COMMAND="docker create -i -t \
             -e HOST_UID=$(id -u $USER) \
             -e HOST_GID=$(id -g $USER) \
             -e DISPLAY=unix$DISPLAY \
             --device /dev/dri \
             -v $SHARED_DIR_HOST:$SHARED_DIR_CONTAINER \
             -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
             --name dumux \
             $IMAGE /bin/bash"

    CONTAINER_NAME=$(executeandlog $COMMAND | tail -n 1)
    executeandlog docker start -a -i $CONTAINER_NAME
    executeandlog docker rm $CONTAINER_NAME
}

# Check if user specified valid command otherwise print help message
if [ "$1" == "open" ]; then
    IMAGE="$2" : ${IMAGE:="$PUB_IMAGE_NAME"}
    open $IMAGE
else
    help
    exit 1
fi
