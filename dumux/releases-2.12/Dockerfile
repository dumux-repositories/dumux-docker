# DuMuX docker container (version release 2.12)
# see https://github.com/phusion/baseimage-docker for information on the base image
# It is Ubuntu LTS customized for better Docker compatibility
FROM phusion/baseimage:0.9.22
MAINTAINER Timo Koch <timo.koch@iws.uni-stuttgart.de>

# run Ubuntu update as advised on https://github.com/phusion/baseimage-docker
RUN apt-get update \
    && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install the basic dependencies
RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    python-dev \
    python-pip \
    git \
    pkg-config \
    build-essential \
    gfortran \
    cmake \
    mpi-default-bin \
    mpi-default-dev \
    libsuitesparse-dev \
    libsuperlu-dev \
    libeigen3-dev \
    doxygen \
    paraview \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# add the permission helper script to the my_init service
COPY setpermissions.sh /etc/my_init.d/setpermissions.sh

# create a dumux user
# add the welcome message (copied further down) output to bashrc
# make the set permission helper script executable
# add user to video group which enables graphics if desired
RUN useradd -m --home-dir /dumux dumux \
    && echo "cat /dumux/WELCOME" >> /dumux/.bashrc \
    && chmod +x /etc/my_init.d/setpermissions.sh \
    && usermod -a -G video dumux

# Turn off verbose syslog messages as described here:
# https://github.com/phusion/baseimage-docker/issues/186
RUN touch /etc/service/syslog-forwarder/down

# switch to the dumux user and set the working directory
USER dumux
WORKDIR /dumux

# create a shared volume communicating with the host
RUN mkdir /dumux/shared
VOLUME /dumux/shared

# This is the message printed on entry
COPY WELCOME /dumux/WELCOME

# clone dune dependencies
RUN git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-common.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-geometry.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-grid.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-istl.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-localfunctions.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/staging/dune-uggrid.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/extensions/dune-alugrid.git && \
    git clone -b releases/2.5 https://gitlab.dune-project.org/extensions/dune-foamgrid.git

# clone dumux repository
RUN git clone -b releases/2.12 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

# configure module
RUN /dumux/dune-common/bin/dunecontrol --opts=/dumux/dumux/optim.opts all

# build doxygen documentation
RUN cd dumux/build-cmake && make doc

# switch back to root
USER root

# make graphical output with paraview work
ENV QT_X11_NO_MITSHM 1

# set entry point like advised https://github.com/phusion/baseimage-docker
# this sets the permissions right, see above
ENTRYPOINT ["/sbin/my_init","--quiet","--","/sbin/setuser","dumux","/bin/bash","-l","-c"]

# start interactive shell
CMD ["/bin/bash","-i"]
