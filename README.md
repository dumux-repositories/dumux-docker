Using DuMux in a Docker container
==================================

This repository contains the Dockerfiles to build Docker images. The Docker
images can be found on the [Dumux Docker Hub](https://hub.docker.com/u/dumux/).
Using these Docker images you can spin up Docker containers to run DuMux.


Running a DuMux Docker container
---------------------------------

You also can spin up a Docker container running DuMux by simly using the Docker command. However,
the expression can get quite lengthy, especially when you want to support the graphical display.

Run
```bash
docker run -it --rm --env DISPLAY=unix$DISPLAY --device /dev/dri --volume /tmp/.X11-unix:/tmp/.X11-unix:rw dumux:latest /bin/bash
```

Running a DuMux Docker container with the script
-------------------------------------------------

In the folder dumux/release-2.12 you are going to find a script called `dumux` that spins up the container when you run

Run
```bash
./dumux open
```

Running with docker-compose
-------------------------------

The easiest is starting the container using docker-compose.
Create a YAML file called `docker-compose.yml` (default recognized by docker-compose) or choosing
a custom name with the following content.

```yaml
# Starts a dumux container with graphic support
dumux:
  image: dumux:latest
  container_name: 'dumux'
  volumes:
    - /tmp/.X11-unix:/tmp/.X11-unix:rw
  devices:
    - /dev/dri
  environment:
    - DISPLAY=unix$DISPLAY
  entrypoint:
    - /bin/bash
  stdin_open: true
  tty: true
```

Then run

```bash
docker-compose [-f mydockercomposefile.yml] run dumux
```
This will start a Docker container running DuMux (hopefully with graphical support).
Docker-compose will default to `docker-compose.yml` if no file is specified with the `-f` option.


Run first DuMux program in Docker container
---------------------------------------------

To run your first DuMux application change the directory by running
```bash
cd dumux/build-cmake/test/porousmediumflow/1p/implicit
```
build the program running
```bash
make test_cc1p
```
and run it
```bash
./test_cc1p
```

Congratulations you just solved a one-phase model in a quadratic domain with permeability lens.
You can have a look at the result with the visualization tool paraview.

```bash
paraview 1ptestcc.pvd
```
