# only for me locally to build new images
# when uploading new images also pull them on the build worker
docker build -t timokoch/bb-dumux-master-pdelab:latest -f bb-dumux-master-pdelab/Dockerfile .
docker build -t timokoch/bb-dumux-release-pdelab:latest -f bb-dumux-release-pdelab/Dockerfile .
docker build -t timokoch/bb-dumux-master:latest -f bb-dumux-master/Dockerfile .
docker build -t timokoch/bb-dumux-release:latest -f bb-dumux-release/Dockerfile .
docker build -t timokoch/bb-dumux-next:latest -f bb-dumux-next/Dockerfile .
docker build -t timokoch/bb-lecture-master:latest -f bb-lecture-master/Dockerfile .
docker build -t timokoch/bb-lecture-release:latest -f bb-lecture-release/Dockerfile .

docker push timokoch/bb-dumux-master-pdelab:latest
docker push timokoch/bb-dumux-release-pdelab:latest
docker push timokoch/bb-dumux-master:latest
docker push timokoch/bb-dumux-release:latest
docker push timokoch/bb-dumux-next:latest
docker push timokoch/bb-lecture-master:latest
docker push timokoch/bb-lecture-release:latest

docker rmi timokoch/bb-dumux-master-pdelab:latest
docker rmi timokoch/bb-dumux-release-pdelab:latest
docker rmi timokoch/bb-dumux-master:latest
docker rmi timokoch/bb-dumux-release:latest
docker rmi timokoch/bb-dumux-next:latest
docker rmi timokoch/bb-lecture-master:latest
docker rmi timokoch/bb-lecture-release:latest
