# A latent buildbot worker with ubuntu
# for testing merge requests for dumux-lecture
# the core modules / dune dependencies are release 2.5
FROM ubuntu:xenial
MAINTAINER timokoch <timo.koch@iws.uni-stuttgart.de>

# get the package list
RUN apt-get update && apt-get dist-upgrade --no-install-recommends --yes && \
    apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    python-dev \
    python-pip \
    libffi-dev \
    git \
    pkg-config \
    wget \
    build-essential \
    clang-3.8 \
    gfortran \
    cmake \
    mpi-default-dev \
    mpi-default-bin \
    libsuitesparse-dev \
    libmetis-dev \
    libsuperlu-dev \
    libscotchparmetis-dev \
    zlib1g-dev \
    libboost-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# create a buildbot user
RUN useradd --create-home --home-dir /data/ buildbot

# create system-wide symlink to dunecontrol for convenience
RUN ln -s /data/src/dune-common/bin/dunecontrol /usr/bin/dunecontrol

# install and set up a virtualenv
RUN pip install virtualenv six

# switch to buildbot user
USER buildbot
RUN virtualenv /data/buildbot
WORKDIR /data/buildbot

# install buildbot master using pip in a virtual env
RUN ./bin/pip install --upgrade pip six
RUN ./bin/pip install 'buildbot-worker==0.9.0b9'

# The files in worker get configured by environmental variables at runtime
# Build this image in the same folder as the worker folder
ADD worker /data/buildbot/worker
ADD opts /data/opts
USER root
RUN chown -R buildbot:buildbot /data/buildbot/worker && \
    chown -R buildbot:buildbot /data/opts
USER buildbot

# create source directory
RUN mkdir -p /data/src
WORKDIR /data/src/
RUN git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-common.git && \
    git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-geometry.git && \
    git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-grid.git && \
    git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-istl.git && \
    git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-localfunctions.git && \
    git clone -b releases/2.4 https://gitlab.dune-project.org/extensions/dune-alugrid.git && \
    git clone -b releases/2.0 git://github.com/smuething/dune-multidomain.git && \
    git clone -b releases/2.3 git://github.com/smuething/dune-multidomaingrid.git && \
    git clone -b releases/2.0 https://gitlab.dune-project.org/pdelab/dune-pdelab.git && \
    git clone -b releases/2.3 https://gitlab.dune-project.org/staging/dune-typetree.git && \
    git clone -b releases/2.10 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git && \
    ./dumux/bin/installexternal.sh gstat

# start worker
WORKDIR /data/buildbot/
CMD ["./bin/buildbot-worker", "start", "--nodaemon", "worker"]
